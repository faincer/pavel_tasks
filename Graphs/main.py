from mindy.graph import basics as bs
import config
from mindy.graph import neo4j_graph as pg

def makeNewNode(guessAnimal:bs.Graph, prevAnimal: str)->bs.Graph:
    print('Сдаюсь что это?')
    newAnimal=input().lower()
    print('В чем отличие животного('+newAnimal+') от животного('+
        prevAnimal[:-1].lower()+')?')
    newNode=input().lower()+'?'
    prevQuest=guessAnimal.MatchOne({'print':prevAnimal}).Parent({})
    prevQuest.Child({}).Delete()
    prevQuest.Connect(
        bs.Node(guessAnimal,{'print':newNode,prevQuest['print']:'да'})
    )
    guessAnimal.MatchOne({'print':newNode}).Connect(
        bs.Node(guessAnimal,{'print':prevAnimal,newNode:'нет'})
    )
    guessAnimal.MatchOne({'print':newNode}).Connect(
        bs.Node(guessAnimal,{'print':newAnimal,newNode:'да'})
    )
    return guessAnimal
    



def makeGraph()->bs.Graph:
    guessAnimal=bs.Graph()
    startGame=bs.Node(guessAnimal,{'print': 'Сыграем в игру?','start':1})
    firstNode=bs.Node(guessAnimal,{'print':'Птица?','Сыграем в игру?':'да'})
    animal1=bs.Node(guessAnimal,{'print':'Голубь?','Птица?':'да'})
    animal2=bs.Node(guessAnimal,{'print':'Кот?','Птица?':'нет'})
    endGame=bs.Node(guessAnimal,{'print':'Тогда пока:)',
                    'Сыграем в игру?':'нет'})
    winPrint=bs.Node(guessAnimal,{'print':'Ура, я выиграл:)', 'answer': 'да'})
    startGame.Connect(firstNode)
    startGame.Connect(endGame)
    firstNode.Connect(animal1)
    firstNode.Connect(animal2)
    animal1.Connect(winPrint)
    animal2.Connect(winPrint)
    winPrint.Connect(startGame)
    return guessAnimal
        
def game():
    gsAnml=makeGraph()
    question=gsAnml.MatchOne({'start':1})['print']
    answer=''    
    while answer!='exit':
        print(question)
        if question=='Тогда пока:)':
            break
        answer=input().lower()
        if gsAnml.Match({question:answer}):
            question=gsAnml.MatchOne({question:answer})['print']
        elif answer=='нет':
            gsAnml=makeNewNode(gsAnml,question)
            print('Окей, запомню.')
            question=gsAnml.MatchOne({'start':1})['print']
        elif answer=='да':
            print(gsAnml.MatchOne({'answer':answer})['print'])
            question=gsAnml.MatchOne({'start':1})['print']
        else:
            print('Я тебя не понимаю(\n...')

            


if __name__ == "__main__":
    game()
    graph =  pg.PersistentGraph(
                config.graph_connection, graph_segment="mygraph")
    #n = bg.Node(graph,{"type":"test_node"})
