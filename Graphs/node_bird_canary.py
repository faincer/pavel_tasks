from mindy.graph import basics as bs
from mindy.graph import configex
from mindy.graph import neo4j_graph as pg


if __name__ == "__main__":
    graph =  pg.PersistentGraph(
                configex.graph_connection, graph_segment="mygraph")
    bird = bs.Node(graph,{"type":"bird"})
    canary = bs.Node(graph,{"type":"canary","can_fly":"no"})
    bird.Connect(canary)