from tco import *
from typing import List
from random import randint

# рекурсионная функция
def splt(x: str)->List[str]: 
        """Превращает строку x вида "Это строка" в лист - ["Это","строка"]"""
        return splt(x[:x.rfind(" ")])+[
                x[x.rfind(" ")+1:]] if x.rfind(" ")!=-1 else [x]

def keep_sep_head(x: str, lst_sepi: int)->List:
    "Добавляет в конец <STOP>"
    if x[lst_sepi] in [".","?","!"]:
        result = [(x[lst_sepi],lst_sepi,lst_sepi),("<STOP>",-1,-1)]
    else:
        result = [(x[lst_sepi],lst_sepi,lst_sepi)] if not x[lst_sepi+1:]!='' else [
            (x[lst_sepi],lst_sepi,lst_sepi),(x[lst_sepi+1:],lst_sepi+1,len(x)-1)] 
    return result

def rsplit(x: str, seps: List[str], keep_sep: bool=True, get_pos: bool=True)->List[str]: 
    """Прямая рекурсионная функция. Превращает строку x в лист слов с помощью разделителей
    Пример: keep_sep=False: "Это, строка." => ["Это","строка"]
    keep_sep=True: "Это, строка." => ["Это",",","строка"]
    get_pos=False: "Это, строка." => ["Это",",","строка"]
    get_pos=True: "Это, строка." => [("Это",0,3),(",",4,4),("строка",5,10)]
    Arguments:
        string -- строка вида "Это, строка."
        seps -- разделители, которые нельзя вводить
        keep_sep -- флажок, показывающий сохранить разделители или нет
        get_pos -- флажок, включающий вывод позиции, где находится слово
    Return:
        list_x -- лиcт вида ["Это","строка"]
    """
    # lst_sepi = last separator index
    lst_sepi=max([x.rfind(s) for s in seps])
    if keep_sep:
        if not get_pos:
            head=lambda x: [x[lst_sepi],x[lst_sepi+1:]] if not x[lst_sepi+1:] in [""," "] else [x[lst_sepi]]
        else:
            head=lambda x: keep_sep_head(x,lst_sepi)
    else:
        head=lambda x: [x[lst_sepi+1:] if not get_pos else (x[lst_sepi+1:],
                    lst_sepi+1,len(x)-1)] if x[lst_sepi+1:]!="" else []
    return rsplit(x[:lst_sepi],seps,keep_sep,get_pos)+head(x) if lst_sepi!=-1 else [
        x if not get_pos else (x,0,len(x)-1)]

@with_continuations()
def rsplit_tail(string: str, seps: List[str]=[" "], keep_sep: bool=True, self=None)->List[str]: 
    """Обратная рекурсионная функция. Превращает строку x в лист слов с помощью разделителей
    Пример: keep_sep=False: "Это, строка." => ["Это","строка"]
    keep_sep=True: "Это, строка." => ["Это",",","строка"]
    Arguments:
        string -- строка вида "Это, строка."
        seps -- разделители, которые надо заменить на пробелы
        keep -- флажок, показывающий сохранить разделители или нет
    Return:
        list_x -- лиcт вида ["Это","строка"]
    """
    return self(string.replace(seps[0], " "+seps[0]) 
        if keep_sep else string.replace(seps[0], " "),seps[1:],keep_sep
            ) if seps!=[] else [x for x in splt(string) if x!=""]
    
if  __name__ == "__main__":
    print(rsplit("hello, there!", [" ",",","!",".","?",":",";"],True,True))
    print(rsplit_tail("hello, there!", [" ",",","!"],True))